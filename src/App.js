import DemoApp from './components/DemoApp'
import './bootstrap.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <DemoApp/>
    </div>
  );
}

export default App;
