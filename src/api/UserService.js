import axios from 'axios'

class UserService {
    get(name) {
        return axios.get(`http://localhost:8080/users/${name}`)
    }
    post(name, age) {
        const body = {
            name: name,
            age: age
        }
        return axios.post(`http://localhost:8080/users`, body)
    }
    update(name, age) {
        const body = {
            name: name,
            age: age
        }
        return axios.put(`http://localhost:8080/users`, body)
    }
    delete(name) {
        return axios.delete(`http://localhost:8080/users/${name}`)
    }
}

export default new UserService();