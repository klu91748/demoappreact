import React, {Component} from 'react'
import UserService from '../../src/api/UserService.js'

class Delete extends Component {
    constructor() {
        super()
        this.state = {
            nameInput: '',
            message: ''
        }
        this.delete = this.delete.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    delete() {              
        UserService.delete(this.state.nameInput, this.state.ageInput)
            .then((e) => {
                this.setState({message: `User has been deleted.`})})
            .catch(this.setState({message: 'User does not exists!'}))
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div class="form-group mx-auto w-25 h-50">
                <br></br>
                <input onChange={this.handleChange} type="text" name="nameInput" placeholder="Enter name"/>
                <button class="btn btn-success" onClick={this.delete}>Submit</button>
                <br></br>
                <h1>{this.state.message}</h1>
            </div>
        )
    }
}

export default Delete