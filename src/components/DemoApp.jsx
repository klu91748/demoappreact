import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Header from './Header'
import Home from './Home'
import Get from './Get'
import Post from './Post'
import Update from './Update'
import Delete from './Delete'

class DemoApp extends Component {
    render() {
        return (
            <div className="DemoApp">
                <Router>
                    <Header/>
                        <Switch>                          
                            <Route path="/" exact component={Home}/>
                            <Route path="/home" component={Home}/>
                            <Route path="/get" component={Get}/>
                            <Route path="/post" component={Post}/>
                            <Route path="/update" component={Update}/>
                            <Route path="/delete" component={Delete}/>
                        </Switch>
                </Router>
            </div>
        )
    }
}

export default DemoApp