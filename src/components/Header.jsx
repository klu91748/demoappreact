import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class Header extends Component {
    render() {
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                <ul className="navbar-nav">
                        {<li><Link className="nav-link" to="/home">Home</Link></li>}
                        {<li><Link className="nav-link" to="/get">Get</Link></li>}
                        {<li><Link className="nav-link" to="/post">Post</Link></li>}
                        {<li><Link className="nav-link" to="/update">Update</Link></li>}
                        {<li><Link className="nav-link" to="/delete">Delete</Link></li>}
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header