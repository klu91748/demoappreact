import React, {Component} from 'react'

class Home extends Component {

    constructor() {
        super()
        this.state = {
            phrase: "Welcome!"
        }
    }

    render() {
        return (
            <div>
                <h1>{this.state.phrase}!</h1>
            </div>
        )
    }
}

export default Home