import React, {Component} from 'react'
import UserService from '../../src/api/UserService.js'

class Update extends Component {

    constructor() {
        super()
        this.state = {
            nameInput: '',
            ageInput: '',
            message: ''
        }
        this.update = this.update.bind(this)
        this.handleChange = this.handleChange.bind(this)      
    }

    update() {
        UserService.update(this.state.nameInput, this.state.ageInput)
            .then((e) => {
                this.setState({message: `Name: ${e.data.name}'s age is updated to 
                ${e.data.age}!`})})
            .catch(this.setState({message: 'User does not exists!'}))
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })

    }
    
    render() {
        return (
            <div class="form-group mx-auto w-25 h-50">
                <br></br>
                <input onChange={this.handleChange} type="text" name="nameInput" placeholder="Enter name"></input>
                <input onChange={this.handleChange} type="number" name="ageInput" placeholder="Enter age"></input>
                <button class="btn btn-success" onClick={this.update}>Submit</button>
                <br></br>
                <h1>{this.state.message}</h1>
            </div>
        )
    }
}

export default Update